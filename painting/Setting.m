//
//  Setting.m
//  painting
//
//  Created by Thao on 7/14/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import "Setting.h"
#import "ViewController.h"

@interface Setting ()

@end

float red;
float green;
float blue;
float opacity;
float brush;

@implementation Setting
    


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    _sliBrush.value = brush;
    _sliOpacity.value = opacity;
    _sliBlue.value = blue;
    _sliGreen.value = green;
    _sliRed.value = red;
    _lblRed.text = [NSString stringWithFormat:@"%.0f", _sliRed.value];
    _lblGreen.text = [NSString stringWithFormat:@"%.0f", _sliGreen.value];
    _lblBlue.text = [NSString stringWithFormat:@"%.0f", _sliBlue.value];
    _lblBrush.text = [NSString stringWithFormat:@"%.0f", _sliBrush.value];
    _lblRed.text = [NSString stringWithFormat:@"%.0f", _sliRed.value];
    _lblOpacity.text = [NSString stringWithFormat:@"%.0f%@", _sliOpacity.value*100, @"%"];
    UIColor *color =[UIColor colorWithRed:_sliRed.value/255.f green:_sliGreen.value/255.f blue:_sliBlue.value/255.f alpha:_sliOpacity.value];
    _imgColor.backgroundColor=color;
    
    CGRect c = self.imgColor.bounds;
    c.size.height = _sliBrush.value;
    [self.imgColor setBounds:c];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnOK:(id)sender {
    brush = _sliBrush.value;
    red = _sliRed.value;
    green = _sliGreen.value;
    blue = _sliBlue.value;
    opacity = _sliOpacity.value;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)sliRedSlide:(id)sender {
    _lblRed.text = [NSString stringWithFormat:@"%.0f", _sliRed.value];
    UIColor *color =[UIColor colorWithRed:_sliRed.value/255.f green:_sliGreen.value/255.f blue:_sliBlue.value/255.f alpha:_sliOpacity.value];
    _imgColor.backgroundColor=color;
}

- (IBAction)sliGreenSlide:(id)sender {
    _lblGreen.text = [NSString stringWithFormat:@"%.0f", _sliGreen.value];
    UIColor *color =[UIColor colorWithRed:_sliRed.value/255.f green:_sliGreen.value/255.f blue:_sliBlue.value/255.f alpha:_sliOpacity.value];
    _imgColor.backgroundColor=color;
}

- (IBAction)sliBlueSlide:(id)sender {
    _lblBlue.text = [NSString stringWithFormat:@"%.0f", _sliBlue.value];
    UIColor *color =[UIColor colorWithRed:_sliRed.value/255.f green:_sliGreen.value/255.f blue:_sliBlue.value/255.f alpha:_sliOpacity.value];
    _imgColor.backgroundColor=color;
}

- (IBAction)sliOpacitySlide:(id)sender {
    _lblOpacity.text = [NSString stringWithFormat:@"%.0f%@", _sliOpacity.value*100,@"%"];
    UIColor *color =[UIColor colorWithRed:_sliRed.value/255.f green:_sliGreen.value/255.f blue:_sliBlue.value/255.f alpha:_sliOpacity.value];
    _imgColor.backgroundColor=color;
}

- (IBAction)sliBrushSlide:(id)sender {
    _lblBrush.text = [NSString stringWithFormat:@"%.0f", _sliBrush.value];
//    UIColor *color =[UIColor colorWithRed:_sliRed.value/255.f green:_sliGreen.value/255.f blue:_sliBlue.value/255.f alpha:_sliOpacity.value];
//    _imgColor.backgroundColor=color;
    
    CGRect c = self.imgColor.bounds;
    c.size.height = _sliBrush.value;
    self.imgColor.bounds = c;
}
@end
