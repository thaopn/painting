//
//  ViewController.h
//  painting
//
//  Created by Thao on 7/13/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgPaint;

extern float red;
extern float green;
extern float blue;
extern float opacity;
extern float brush;



- (IBAction)btnBlack:(id)sender;
- (IBAction)btnBlue:(id)sender;
- (IBAction)btnBrown:(id)sender;
- (IBAction)btnDarkGreen:(id)sender;
- (IBAction)btnDarkOrange:(id)sender;
- (IBAction)btnGrey:(id)sender;
- (IBAction)btnLightBlue:(id)sender;
- (IBAction)btnLightGreen:(id)sender;
- (IBAction)btnRed:(id)sender;
- (IBAction)btnYellow:(id)sender;
- (IBAction)btnErase:(id)sender;
- (IBAction)btnSetting:(id)sender;
@end
