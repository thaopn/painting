//
//  Setting.h
//  painting
//
//  Created by Thao on 7/14/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Setting : UIViewController

@property (weak, nonatomic) IBOutlet UISlider *sliBrush;
@property (weak, nonatomic) IBOutlet UISlider *sliOpacity;
@property (weak, nonatomic) IBOutlet UISlider *sliRed;
@property (weak, nonatomic) IBOutlet UISlider *sliGreen;
@property (weak, nonatomic) IBOutlet UISlider *sliBlue;
@property (weak, nonatomic) IBOutlet UILabel *lblBrush;
@property (weak, nonatomic) IBOutlet UILabel *lblOpacity;
@property (weak, nonatomic) IBOutlet UILabel *lblRed;
@property (weak, nonatomic) IBOutlet UILabel *lblGreen;
@property (weak, nonatomic) IBOutlet UILabel *lblBlue;
@property (weak, nonatomic) IBOutlet UIImageView *imgColor;
- (IBAction)btnOK:(id)sender;
- (IBAction)btnClose:(id)sender;

@property (assign, nonatomic) float red;
@property (assign, nonatomic) float green;
@property (assign, nonatomic) float blue;
@property (assign, nonatomic) float opacity;


- (IBAction)sliRedSlide:(id)sender;
- (IBAction)sliGreenSlide:(id)sender;
- (IBAction)sliBlueSlide:(id)sender;
- (IBAction)sliOpacitySlide:(id)sender;
- (IBAction)sliBrushSlide:(id)sender;
@end
