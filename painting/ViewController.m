//
//  ViewController.m
//  painting
//
//  Created by Thao on 7/13/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import "ViewController.h"
#import "Setting.h"



@interface ViewController ()

@end

@implementation ViewController{
    CGPoint currentPoint;
    CGPoint lastPoint;

}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    opacity = 1;
    brush = 1;
    
}

-(void)viewWillAppear:(BOOL)animated{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    currentPoint = [touch locationInView:self.view];
//    lastPoint = currentPoint;
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    lastPoint = [touch locationInView:self.view];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [self.imgPaint.image drawInRect:CGRectMake(0, 0, self.imgPaint.frame.size.width, self.imgPaint.frame.size.height)];
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red/255.f, green/255.f, blue/255.f, opacity);
    //CGContextSetStrokeColor(<#CGContextRef context#>, <#const CGFloat *components#>)
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    _imgPaint.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    currentPoint = lastPoint;
}

- (IBAction)btnBlack:(id)sender {
    red = 0;
    green = 0;
    blue=0;
}
- (IBAction)btnBlue:(id)sender{
    red = 0;
    green = 0;
    blue=205;
}
- (IBAction)btnBrown:(id)sender{
    red = 59;
    green = 29;
    blue=17;
}
- (IBAction)btnDarkGreen:(id)sender{
    red = 54;
    green = 101;
    blue=0;
}
- (IBAction)btnDarkOrange:(id)sender{
    red = 236;
    green = 76;
    blue=0;
}
- (IBAction)btnGrey:(id)sender{
    red = 104;
    green = 104;
    blue=104;
}
- (IBAction)btnLightBlue:(id)sender{
    red = 54;
    green = 201;
    blue=255;
}
- (IBAction)btnLightGreen:(id)sender{
    red = 101;
    green = 255;
    blue=0;
}
- (IBAction)btnRed:(id)sender{
    red = 255;
    green = 0;
    blue = 0;
}
- (IBAction)btnYellow:(id)sender{
    red = 255;
    green = 255;
    blue=0;
}
- (IBAction)btnErase:(id)sender{
    
}

- (IBAction)btnSetting:(id)sender {
    Setting *view = [self.storyboard instantiateViewControllerWithIdentifier:@"Setting"];
    [self presentViewController:view animated:YES completion:nil];
}
@end
